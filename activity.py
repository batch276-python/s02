# Activity 1: Accept a year input from the user and determine if it is a leap year or not. 
# Stretch Goal: Add a validation for the leap year input:
# Strings are not allowed for inputs
# No zero or negative values


while True:
    try:
        year = int(input("Please input a year: "))
        print(year)

        if year <= 0:
            print("No zero or negative values")

        elif year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
            print(year, "is a leap year")
            break
        else:
            print(year, "is not a leap year")
            break

    except ValueError:
            print("Strings are not allowed for inputs")
            



# Activity 2: Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).


row = int(input("Enter number of rows: "))
print(row)

column = int(input("Enter number of colums: "))
print(column)


i = 0
while i < row :
    print(column * "*")
    i += 1
